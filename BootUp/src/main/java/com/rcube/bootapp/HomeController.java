package com.rcube.bootapp;

import java.text.DateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Handles requests for the application home page.
 */
@RestController
@RequestMapping(value = "/Boot")
public class HomeController {

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/homeController", method = RequestMethod.GET)
	public List<Employee> home(Locale locale, Model model) {
		logger.info("Wcome home! The client locale is {}.", locale);

		Employee employeeOne = new Employee();
		employeeOne.setEmployeeAge("22");
		employeeOne.setEmployeeId("1212");
		employeeOne.setEmployeeName("Ramesh");
		employeeOne.setEmployeeWork("PE");
		Employee employeeTwo = new Employee();
		employeeTwo.setEmployeeAge("23");
		employeeTwo.setEmployeeId("2121");
		employeeTwo.setEmployeeName("Shan");
		employeeTwo.setEmployeeWork("BA");
		Employee employeeThree = new Employee();
		employeeThree.setEmployeeAge("24");
		employeeThree.setEmployeeId("2211");
		employeeThree.setEmployeeName("Naresh");
		employeeThree.setEmployeeWork("TL");
		Employee employeeFour = new Employee();
		employeeFour.setEmployeeAge("23");
		employeeFour.setEmployeeId("2422");
		employeeFour.setEmployeeName("Ashwin");
		employeeFour.setEmployeeWork("ARCH");

		List<Employee> employeeList = Arrays.asList(employeeOne, employeeTwo, employeeThree, employeeFour);

		return employeeList;
	}

}
